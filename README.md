# Flace
Завдання для фіналу **IT-REVOLUTION`17**.

## Опис
Додаток призначений для людей, котрі не уявляють своє життя без фотографій, а особливо без селфі.

Користувач може побачати на карті в своєму радіусі цікаві місця, де можна зробити дійсно крутяцьке фото всім на заздрість! ;)

Також він може оцінити кожне місце, побачити усі місця у формі списку, котрий можна фільтрувати за *рейтингом*, *дистанцією*, а також за *тегами*.

Тегів в додатку 5: *Romantic, Extreme, Relax, Sport, Art*.

Звісно кожен користувач може додавати свої місця на мапі, котрі побачать інші.

## [Презентація](https://docs.google.com/presentation/d/1R7ypv5fdq2Rv9yZuHFR6ct8oIyrGuIbwNKx-SfsbWf8/edit?usp=sharing)

## Екрани додатку

1. Головний екран.

![places nearby](https://i.imgur.com/N5pCgQp.png)

2. Створення фіда.

![create place](https://i.imgur.com/TU6r6hw.png)

![select image](https://i.imgur.com/zc4XJLh.png)

![image selected](https://i.imgur.com/OJk8mcg.png)

![select location](https://i.imgur.com/Fw06aKH.png)

![filled fields](https://i.imgur.com/tPyb8bQ.png)

3. Список фідів.

![rating list](https://i.imgur.com/Rq4TOld.png)

4. Оцінювання фіда.

![rate](https://i.imgur.com/8vDfy83.png)

![rated](https://i.imgur.com/h8yt0x0.png)

5. Перегляд фото.

![image preview](https://i.imgur.com/wxW6NgW.jpg)

6. Перегляд локації.

![show locatoin](https://i.imgur.com/QOF1maM.png)
